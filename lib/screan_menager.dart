import 'package:flutter/material.dart';
import 'package:zfl/main_menu.dart';
import 'package:zfl/table/table.dart';
import 'package:zfl/results.dart';
import 'package:zfl/schedule.dart';
import 'package:zfl/teams.dart';
import 'package:zfl/statistics.dart';

class Screan extends StatefulWidget {
  const Screan({super.key});
  @override
  State<Screan> createState() {
    return _ScreanState();
  }
}

String activeScrean = 'main-menu';

class _ScreanState extends State<Screan> {
  void chosenFromMainMenu(String pick) {
    setState(() {
      activeScrean = pick;
    });
  }

  void chosenReturn() {
    setState(() {
      activeScrean = 'main-menu';
    });
  }

  @override
  Widget build(context) {
    Widget screenWidget = MainMenu(chosenFromMainMenu);
    if (activeScrean == 'tablica') {
      screenWidget = Table1(chosenReturn);
    }
    if (activeScrean == 'rezultati') {
      screenWidget = Results(chosenReturn);
    }
    if (activeScrean == 'raspored') {
      screenWidget = Schedule(chosenReturn);
    }
    if (activeScrean == 'ekipe') {
      screenWidget = Teams(chosenReturn);
    }
    if (activeScrean == 'statistika') {
      screenWidget = Statistics(chosenReturn);
    }
    return MaterialApp(
      home: Scaffold(
        body: Container(child: screenWidget),
      ),
    );
  }
}
