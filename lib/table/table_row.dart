import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class TableRow1 extends StatelessWidget {
  const TableRow1(this.index, {super.key});
  final int index;
  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = TextStyle(fontSize: 11);
    return Container(
      width: double.infinity,
      height: 30,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.black, width: 0.5),
        color: Colors.white,
      ),
      child: Row(
        children: [
          Container(
            alignment: Alignment.center,
            width: 30,
            height: 30,
            color: Colors.blue,
            child: Text(
              (index + 1).toString(),
              style: TextStyle(color: Colors.white),
            ),
          ),
          SizedBox(width: 20),
          Icon(Icons.badge),
          Text('Team name#${index + 1}', style: textStyle),
          Spacer(),
          Container(
            width: 28,
            child: Text('MP${index + 1}', style: textStyle),
          ),
          SizedBox(
            width: 5,
          ),
          Container(
            width: 28,
            child: Text('W${index + 1}', style: textStyle),
          ),
          SizedBox(
            width: 5,
          ),
          Container(
            width: 28,
            child: Text('D${index + 1}', style: textStyle),
          ),
          SizedBox(
            width: 5,
          ),
          Container(
            width: 28,
            child: Text('L${index + 1}', style: textStyle),
          ),
          SizedBox(
            width: 5,
          ),
          Container(
            width: 28,
            child: Text('G${index + 1}', style: textStyle),
          ),
          SizedBox(
            width: 5,
          ),
          Container(
            width: 28,
            child: Text('GD${index + 1}', style: textStyle),
          ),
          SizedBox(
            width: 5,
          ),
          Container(
            width: 28,
            child: Text('Pts${index + 1}', style: textStyle),
          ),
          SizedBox(
            width: 5,
          ),
        ],
      ),
    );
  }
}
