import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'top_row.dart';
import 'table_row.dart';

class Table1 extends StatefulWidget {
  const Table1(this.onReturnPressed, {super.key});
  final void Function() onReturnPressed;
  State<Table1> createState() {
    return _Table1State();
  }
}

var selectedYear = 'Sezona 2023/2024';
var selectedRound = 'Round 1';

class _Table1State extends State<Table1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tablica'),
      ),
      body: Column(
        children: [
          DropdownButton<String>(
            value: selectedYear,
            items: ['Sezona 2023/2024', 'Sezona 2024/2025'].map((value) {
              return DropdownMenuItem(
                value: value,
                child: Text(value),
              );
            }).toList(),
            onChanged: (value) {
              setState(() {
                if (value == null) {
                  return;
                }
                selectedYear = value;
              });
            },
          ),
          Expanded(
            child: Align(
              alignment: Alignment.topCenter,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TopRow(),
                    ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: 6,
                      itemBuilder: (context, index) {
                        return TableRow1(index);
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: IconButton(
                onPressed: widget.onReturnPressed,
                icon: const Icon(Icons.home),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
