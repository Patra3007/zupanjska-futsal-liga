import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class TopRow extends StatelessWidget {
  const TopRow({super.key});

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = TextStyle(fontSize: 11);

    return Row(
      children: [
        Container(
          alignment: Alignment.center,
          width: 30,
          height: 30,
          child: Text('Br.'),
        ),
        SizedBox(width: 20),
        Container(alignment: Alignment.center, child: Text('Momčad')),
        Spacer(),
        Container(
          width: 28,
          child: Text('MP', style: textStyle),
        ),
        SizedBox(
          width: 5,
        ),
        Container(
          width: 28,
          child: Text('W', style: textStyle),
        ),
        SizedBox(
          width: 5,
        ),
        Container(
          width: 28,
          child: Text('D', style: textStyle),
        ),
        SizedBox(
          width: 5,
        ),
        Container(
          width: 28,
          child: Text('L', style: textStyle),
        ),
        SizedBox(
          width: 5,
        ),
        Container(
          width: 28,
          child: Text('GS', style: textStyle),
        ),
        SizedBox(
          width: 5,
        ),
        Container(
          width: 28,
          child: Text('GD', style: textStyle),
        ),
        SizedBox(
          width: 5,
        ),
        Container(
          width: 28,
          child: Text('P', style: textStyle),
        ),
        SizedBox(
          width: 5,
        ),
      ],
    );
  }
}
