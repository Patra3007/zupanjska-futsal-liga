import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Statistics extends StatelessWidget {
  const Statistics(this.onReturnPressed, {super.key});
  final void Function() onReturnPressed;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        const Text('Statistics'),
        TextButton.icon(
          label: const Text('HALOOOOOOO'),
          onPressed: onReturnPressed,
          icon: const Icon(Icons.home_filled),
        )
      ],
    );
  }
}
