import 'package:flutter/material.dart';

class MainMenu extends StatefulWidget {
  const MainMenu(
    this.onChosenFromMainMenu, {
    super.key,
  });
  final void Function(String pick) onChosenFromMainMenu;

  @override
  State<MainMenu> createState() {
    return _MainMenuState();
  }
}

class _MainMenuState extends State<MainMenu> {
  void answerQuestion(String pick) {
    widget.onChosenFromMainMenu(pick);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Županjska futsal liga"),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 48),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OutlinedButton(
              onPressed: () {
                answerQuestion('tablica');
              },
              child: const Text('TABLICA'),
            ),
            OutlinedButton(
              onPressed: () {
                answerQuestion('rezultati');
              },
              child: const Text('REZULTATI'),
            ),
            OutlinedButton(
              onPressed: () {
                answerQuestion('raspored');
              },
              child: const Text('RASPORED'),
            ),
            OutlinedButton(
              onPressed: () {
                answerQuestion('ekipe');
              },
              child: const Text('EKIPE'),
            ),
            OutlinedButton(
              onPressed: () {
                answerQuestion('statistika');
              },
              child: const Text('STATISTIKA'),
            ),
          ],
        ),
      ),
    );
  }
}
