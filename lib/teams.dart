import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

var selectedYear = 'Sezona 2023/2024';
List<String> teams = [
  "Ekipa 1",
  "Ekipa 2",
  "Ekipa 3",
  "Ekipa 1",
  "Ekipa 2",
  "Ekipa 3"
];

class Teams extends StatefulWidget {
  const Teams(this.onReturnPressed, {super.key});
  final void Function() onReturnPressed;
  @override
  State<Teams> createState() {
    return _TeamsState();
  }
}

class _TeamsState extends State<Teams> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Ekipe"),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 64),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            DropdownButton<String>(
              value: selectedYear,
              items: ['Sezona 2023/2024', 'Sezona 2024/2025'].map((value) {
                return DropdownMenuItem(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
              onChanged: (value) {
                setState(() {
                  if (value == null) {
                    return;
                  }
                  selectedYear = value;
                });
              },
            ),
            SizedBox(
              height: 500,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    for (final team in teams)
                      Row(
                        children: [
                          TextButton(
                            onPressed: () {},
                            child: Row(
                              children: [
                                Icon(Icons.favorite),
                                const SizedBox(
                                  width: 30,
                                ),
                                Text(team),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 40,
                          ),
                        ],
                      ),
                    const SizedBox(
                      height: 50,
                    )
                  ],
                ),
              ),
            ),
            IconButton(
              onPressed: widget.onReturnPressed,
              icon: const Icon(Icons.home),
            ),
          ],
        ),
      ),
    );
  }
}
