import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Schedule extends StatefulWidget {
  const Schedule(this.onReturnPressed, {super.key});
  final void Function() onReturnPressed;
  @override
  State<Schedule> createState() {
    return _ScheduleState();
  }
}

var date = "25.11.2023";
var selectedYear = 'Sezona 2023/2024';
var selectedRound = 'Round 1';
List<String> team1 = [
  "Ekipa 1",
  "Ekipa 2",
  "Ekipa 3",
];
List<String> team2 = [
  "Ekipa 4",
  "Ekipa 5",
  "Ekipa 6",
];
List<String> time = [
  "20:00",
  "21:00",
  "22:00",
];

class _ScheduleState extends State<Schedule> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Raspored"),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 64),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              children: [
                DropdownButton<String>(
                  value: selectedYear,
                  items: ['Sezona 2023/2024', 'Sezona 2024/2025'].map((value) {
                    return DropdownMenuItem(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  onChanged: (value) {
                    setState(() {
                      if (value == null) {
                        return;
                      }
                      selectedYear = value;
                    });
                  },
                ),
                const SizedBox(
                  width: 30,
                ),
                DropdownButton<String>(
                  value: selectedRound,
                  items: [
                    'Round 1',
                    'Round 2',
                    'Round 3',
                    'Round 4',
                    'Round 5',
                    'Round 6'
                  ].map((value) {
                    return DropdownMenuItem(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  onChanged: (value) {
                    setState(() {
                      if (value == null) {
                        return;
                      }
                      selectedRound = value;
                    });
                  },
                ),
              ],
            ),
            Center(
              child: Text(
                date,
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: 500,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    for (final team in team1)
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Icon(Icons.favorite),
                          const SizedBox(
                            width: 10,
                          ),
                          Text(team),
                          const SizedBox(
                            width: 10,
                          ),
                          Text(time[team1.indexOf(team)]),
                          const SizedBox(
                            width: 10,
                          ),
                          Text(team2[team1.indexOf(team)]),
                          const SizedBox(
                            width: 10,
                          ),
                          Icon(Icons.favorite),
                          const SizedBox(
                            width: 10,
                          ),
                          const SizedBox(
                            height: 100,
                          ),
                        ],
                      ),
                  ],
                ),
              ),
            ),
            IconButton(
              onPressed: widget.onReturnPressed,
              icon: const Icon(Icons.home),
            ),
          ],
        ),
      ),
    );
  }
}
